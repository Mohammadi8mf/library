from rest_framework import viewsets, permissions
from rest_framework_simplejwt.views import TokenObtainPairView
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.filters import SearchFilter

from web.models import CustomUser, Topic, Auther, Book, Loan
from api import serializers, permissions as lib_permissions


class MyTokenObtainPairView(TokenObtainPairView):
    serializer_class = serializers.MyTokenObtainPairSerializer


class UserViewSet(viewsets.ModelViewSet):
    """Handle createing and updating profiles"""

    serializer_class = serializers.UserProfileSerializer
    permission_classes = (lib_permissions.UpdateOwnProfile, permissions.AllowAny)

    def get_queryset(self):
        return CustomUser.objects.filter(id=self.request.user.id).order_by("id")


class TopicViewSet(viewsets.ModelViewSet):
    """Handle """
    serializer_class = serializers.TopicSerializer
    pagination_class = None
    
    def get_queryset(self):
        # print(self.request.user.role)
        return Topic.objects.all()


class AutherViewSet(viewsets.ModelViewSet):
    """Handle """
    serializer_class = serializers.AutherSerializer
    pagination_class = None

    def get_queryset(self):
        return Auther.objects.all()


class BookViewSet(viewsets.ModelViewSet):
    """Handle """
    
    serializer_class = serializers.BookSerializer
    filter_backends = (DjangoFilterBackend, SearchFilter)
    filterset_fields = (
        "auther",
        "topic",
    )
    search_fields = ("title", "auther__auther_name", "topic__topic_name")

    def get_queryset(self):
        return Book.objects.all()


class LoanViewSet(viewsets.ModelViewSet):
    """Handle """
    serializer_class = serializers.LoanSerializer

    def get_queryset(self):
        return Loan.objects.all()